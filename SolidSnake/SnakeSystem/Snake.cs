﻿using SolidSnake.ObstaclesSystem;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using SolidSnake.Utils;
using System.Windows;

namespace SolidSnake.SnakeSystem
{
    [Serializable]
    public class Snake
    {
        public Vector Position { get; set; }
        public int Speed { get; set; }
        private float Offset;
        public int SnakeLifes { get; set; }
        public bool CouldDestroyObstacles { get; set; }
        public Direction currentDirection { get; set; }
        [NonSerialized]
        public Timer SpeedTimer;
        public List<SnakePart> SnakeParts { get; set; }
        private static Random Generator = new Random();

        public Snake(Timer SpeedTimer)
        {
            Position = new Vector(30, 30);
            Speed = 1;
            Offset = 7;
            SnakeLifes = 3;
            currentDirection = Direction.Right;
            SnakeParts = new List<SnakePart>();
            // initialize head
            SnakeParts.Add(new SnakePart(15, 15, Position, null));
            Increase();
            Increase();
            Increase();
            Increase();
            Increase();
            Increase();
            this.SpeedTimer = SpeedTimer;
            CouldDestroyObstacles = false;
        }

        public RectangleF GetRectangle()
        {
            return new RectangleF(SnakeParts[0].GetPoint(), new SizeF(SnakeParts[0].Width, SnakeParts[0].Height));
        }

        /// <summary>
        /// Increasing the body of snake.
        /// </summary>
        public void Increase()
        {
            SnakePart last = SnakeParts[SnakeParts.Count - 1];
            double x = 0, y = 0;
            if (currentDirection == Direction.Up)
            {
                y = last.Position.Y + 15;
                x = last.Position.X;
            }
            if (currentDirection == Direction.Down)
            {
                y = last.Position.Y - 15;
                x = last.Position.X;
            }
            if (currentDirection == Direction.Left)
            {
                y = last.Position.Y;
                x = last.Position.X + 15;
            }
            if (currentDirection == Direction.Right)
            {
                y = last.Position.Y;
                x = last.Position.X - 15;
            }
            SnakePart partToAdd = new SnakePart(15, 15, new Vector(x, y), last);
            SnakeParts.Add(partToAdd);
        }

        /// <summary>
        /// The body of the snake gets smaller. 
        /// </summary>
        public void Shrink()
        {
            if (SnakeParts.Count > 1)
            {
                int ind = SnakeParts.Count - 1;
                SnakeParts.RemoveAt(ind);
            }
        }

        public bool IsEatingItself()
        {
            SnakePart PartOne = SnakeParts[0];
            for (int i = 2; i < SnakeParts.Count; i++)
            {
                if (CollisionDetector.CheckCollision(PartOne, SnakeParts[i]))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Moving all parts of the snake. Each part of the snake moves to the place where his successor was.
        /// </summary>
        /// <param name="width">Horizontal length of the surface where snake can move</param>
        /// <param name="height">Vertical length of the surface where snake can move</param>
        public void Move(int width, int height)
        {
            MoveAllSnakeParts(width, height);
        }

        private void MoveAllSnakeParts(int width, int height)
        {
            double x = SnakeParts[0].Position.X, y = SnakeParts[0].Position.Y;
            if (currentDirection == Direction.Up)
            {
                y += (-Offset);
            }
            if (currentDirection == Direction.Down)
            {
                y += (Offset);
            }
            if (currentDirection == Direction.Left)
            {
                x += (-Offset);
            }
            if (currentDirection == Direction.Right)
            {
                x += (Offset);
            }

            SnakeParts[0].Position = new Vector(x, y);

            if ((x < 0 || x > width - 35))
            {
                ChangeToVerticalDirection(width, height);
            }
            if ((y < 0 || y > height - 55))
            {
                ChangeToHorizontalDirection(width, height);
            }

            for (int i = 1; i < SnakeParts.Count; i++)
            {
                SnakeParts[i].FollowHead();
            }
        }

        public bool checkUpperProximity(Rectangle currentView)
        {
            SnakePart snakeHead = SnakeParts[0];
            double Y = snakeHead.Position.Y;
            return (Y < currentView.Top+15) ? true : false;
        }

        public bool checkBottomProximity(Rectangle currentView)
        {
            SnakePart snakeHead = SnakeParts[0];
            double Y = snakeHead.Position.Y;
            return (Y > currentView.Bottom-25) ? true : false;
        }

        public bool checkLeftProximity(Rectangle currentView)
        {
            SnakePart snakeHead = SnakeParts[0];
            double X = snakeHead.Position.X;
            return (X < currentView.Left+15) ? true : false;
        }

        public bool checkRightProximity(Rectangle currentView)
        {
            SnakePart snakeHead = SnakeParts[0];
            double X = snakeHead.Position.X;
            return (X > currentView.Right - 25) ? true : false;
        }

        private void ChangeToVerticalDirection(int width, int height)
        {
            if (SnakeParts[0].Position.X < 0) SnakeParts[0].Position = new Vector(0, SnakeParts[0].Position.Y);
            if (SnakeParts[0].Position.X > width - 34) SnakeParts[0].Position = new Vector(width - 34, SnakeParts[0].Position.Y);

            if (SnakeParts[0].Position.Y < 50) changeSnakeDirection(Direction.Down);
            else if (SnakeParts[0].Position.Y > height - 100) changeSnakeDirection(Direction.Up);
            else
            {
                int down = Generator.Next(10);
                if (down % 2 == 0) changeSnakeDirection(Direction.Down);
                else changeSnakeDirection(Direction.Up);
            }

        }

        private void ChangeToHorizontalDirection(int width, int height)
        {
            if (SnakeParts[0].Position.Y < 0) SnakeParts[0].Position = new Vector(SnakeParts[0].Position.X, 0);
            if (SnakeParts[0].Position.Y > height - 55) SnakeParts[0].Position = new Vector(SnakeParts[0].Position.X, height - 55);

            if (SnakeParts[0].Position.X < 80) changeSnakeDirection(Direction.Right);
            else if (SnakeParts[0].Position.X > width - 80) changeSnakeDirection(Direction.Left);
            else
            {
                int left = Generator.Next(10);
                if (left % 2 == 0) changeSnakeDirection(Direction.Left);
                else changeSnakeDirection(Direction.Right);
            }
        }

        public void changeSnakeDirection(Direction direction)
        {
            if (!((direction == Direction.Left && currentDirection == Direction.Right)
                || (direction == Direction.Up && currentDirection == Direction.Down)
                || (direction == Direction.Down && currentDirection == Direction.Up)
                || (direction == Direction.Right && currentDirection == Direction.Left)))
            {
                currentDirection = direction;
            }
        }

        public void IncreaseSpeed()
        {
            Offset = 13F;
        }

        public void NormalizeSpeed()
        {
            Offset = 7F;
        }

        public void DecreaseSpeed()
        {
            Offset = 4F;
        }

        public void KillSnake()
        {
            SnakeLifes = 0;
        }
        public void TakeLife()
        {
            SnakeLifes--;
        }

        /// <summary>
        /// Drawing the snake on specified surface.
        /// </summary>
        /// <param name="display">Surface where the snake will be drawn</param>
        public void Draw(Graphics display)
        {
            foreach (var part in SnakeParts)
            {
                part.Draw(display);
            }
        }
    }
}
