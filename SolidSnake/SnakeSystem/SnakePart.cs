﻿using SolidSnake.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows;

namespace SolidSnake.SnakeSystem
{
    [Serializable]
    public class SnakePart
    {
        public float Width { get; set; }
        public float Height { get; set; }
        public Vector Position { get; set; }
        public double DistanceToParent { get; set; }
        public SnakePart Parent { get; set; }
        public static double followDistance = 15;
        public SnakePart(SnakePart parent) {
            Parent = parent;
        }

        public SnakePart(float Width, float Height, Vector Position, SnakePart Parent)
        {
            this.Width = Width;
            this.Height = Height;
            this.Position = Position;
            this.Parent = Parent;
            if (Parent != null)
            {
                var DifferenceToParent = Parent.Position - Position;
                DistanceToParent = DifferenceToParent.Length;
            } else
            {
                DistanceToParent = 15;
            }
        }

        public void Draw(Graphics display)
        {
            Image img = Resources.SnakeSkin3;
            display.FillEllipse(Brushes.LightGreen, new RectangleF(GetPoint(), new SizeF(Width, Height)));
        }

        public void FollowHead()
        {
            var differenceToParent = Parent.Position - Position;
            var distanceToParent = differenceToParent.Length;

            if (distanceToParent > followDistance)
            {
                var tooFar = distanceToParent - followDistance;
                differenceToParent.Normalize();
                var translation = differenceToParent * tooFar;

                Position += translation;
            }
        }

        internal PointF GetPoint()
        {
            return new PointF((float)Position.X, (float)Position.Y);
        }

        public RectangleF GetRectangle()
        {
            return new RectangleF((float)Position.X, (float)Position.Y, Width, Height);
        }
    }
}
