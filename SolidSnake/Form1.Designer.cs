﻿namespace SolidSnake
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SnakeSpeedTimer = new System.Windows.Forms.Timer(this.components);
            this.FoodsTimer = new System.Windows.Forms.Timer(this.components);
            this.pauseDisplay = new System.Windows.Forms.PictureBox();
            this.playButton = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pauseDisplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playButton)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SnakeSpeedTimer
            // 
            this.SnakeSpeedTimer.Interval = 15;
            this.SnakeSpeedTimer.Tick += new System.EventHandler(this.SnakeSpeedTimer_Tick);
            // 
            // FoodsTimer
            // 
            this.FoodsTimer.Interval = 1000;
            this.FoodsTimer.Tick += new System.EventHandler(this.FoodsTimer_Tick);
            // 
            // pauseDisplay
            // 
            this.pauseDisplay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pauseDisplay.Enabled = false;
            this.pauseDisplay.Image = global::SolidSnake.Properties.Resources.Pause;
            this.pauseDisplay.Location = new System.Drawing.Point(0, 0);
            this.pauseDisplay.Name = "pauseDisplay";
            this.pauseDisplay.Size = new System.Drawing.Size(816, 489);
            this.pauseDisplay.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pauseDisplay.TabIndex = 0;
            this.pauseDisplay.TabStop = false;
            this.pauseDisplay.Visible = false;
            this.pauseDisplay.Click += new System.EventHandler(this.pauseDisplay_Click);
            this.pauseDisplay.Paint += new System.Windows.Forms.PaintEventHandler(this.pauseDisplay_Paint);
            this.pauseDisplay.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pauseDisplay_MouseMove);
            // 
            // playButton
            // 
            this.playButton.BackgroundImage = global::SolidSnake.Properties.Resources.btn_img;
            this.playButton.Image = global::SolidSnake.Properties.Resources.PLAY_gifTransparent;
            this.playButton.Location = new System.Drawing.Point(276, 269);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(129, 50);
            this.playButton.TabIndex = 1;
            this.playButton.TabStop = false;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            this.playButton.MouseEnter += new System.EventHandler(this.playButton_MouseEnter);
            this.playButton.MouseLeave += new System.EventHandler(this.playButton_MouseLeave);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::SolidSnake.Properties.Resources.Walpaper3;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1184, 661);
            this.Controls.Add(this.playButton);
            this.Controls.Add(this.pauseDisplay);
            this.DoubleBuffered = true;
            this.MaximumSize = new System.Drawing.Size(1200, 700);
            this.MinimumSize = new System.Drawing.Size(1200, 700);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Solid Snake";
            this.Click += new System.EventHandler(this.Form1_Click);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.DoubleClick += new System.EventHandler(this.Form1_DoubleClick);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseClick);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            ((System.ComponentModel.ISupportInitialize)(this.pauseDisplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playButton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer SnakeSpeedTimer;
        private System.Windows.Forms.Timer FoodsTimer;
        private System.Windows.Forms.PictureBox pauseDisplay;
        private System.Windows.Forms.PictureBox playButton;
    }
}

