﻿using SolidSnake.Properties;
using SolidSnake.SnakeSystem;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidSnake.ObstaclesSystem
{
    [Serializable]
    public class Rocket
    {
        Point Position;
        int Width, Height, velX, velY;
        Direction Direction;
        int Speed;
        Snake Snake;
        public bool ShouldBeDestroyed;

        public Rocket(int x, int y, int width, int height, Direction direction)
        {
            ShouldBeDestroyed = false;
            Position = new Point(x, y);
            Width = width;
            Height = height;
            Direction = direction;
            velX = velY = 0;
            Speed = 3;
            if (Direction == Direction.Down) velY = Speed;
            if (Direction == Direction.Up) velY = -Speed;
            if (Direction == Direction.Left) velX = -Speed;
            if (Direction == Direction.Right) velX = Speed;
        }

        public Rectangle GetRectangle()
        {
            return new Rectangle(Position, new Size(20, 20));
        }

        public void IdentifySnake(Snake snake)
        {
            Snake = snake;
        }

        public void Move()
        {
            int newX = Position.X + velX;
            int newY = Position.Y + velY;
            Position = new Point(newX, newY);

            if(Snake != null)
            {
                if(IsCollideWithSnake())
                {
                    Snake.SnakeLifes = 0;
                    ShouldBeDestroyed = true;
                    return;
                }
            }

            if((newX < 0 || newX > Width) || (newY < 0 || newY > Height))
            {
                ShouldBeDestroyed = true;
            } 
        }

        private bool IsCollideWithSnake()
        {
            foreach(var part in Snake.SnakeParts)
            {
                if(part.GetRectangle().IntersectsWith(GetRectangle())) {
                    return true;
                }
            }
            return false;
        }

        public void Draw(Graphics display)
        {
            Image img = null;
            if (Direction == Direction.Left) img = Resources.Rocket_Left;
            if (Direction == Direction.Right) img = Resources.Rocket_Right;
            if (Direction == Direction.Up) img = Resources.Rocket_Up;
            if (Direction == Direction.Down) img = Resources.Rocket_Down;
            display.DrawImage(img, Position);
        }
    }
}
