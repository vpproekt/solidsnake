﻿using System;
using System.Drawing;
using SolidSnake.Properties;
using SolidSnake.SnakeSystem;

namespace SolidSnake.ObstaclesSystem
{
    [Serializable]
    public class GameOverObstacles : Obstacle
    {
        public GameOverObstacles()
        {
            Width = 54;
            Height = 41;
        }

        public override int AcquirePoints()
        {
            return 15;
        }

        public override Obstacle Clone()
        {
            return new GameOverObstacles();
        }

        public override void Draw(Graphics display)
        {
            Image img = Resources.GameOver170;
            display.DrawImage(img, Position);
            //display.DrawRectangle(Pens.Red, new Rectangle(Position, new Size(Width, Height)));
        }

        public override void Punish(Snake TheSnake)
        {
            TheSnake.KillSnake();
        }
    }
}
