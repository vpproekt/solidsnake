﻿using System;
using System.Drawing;
using SolidSnake.Properties;
using SolidSnake.SnakeSystem;

namespace SolidSnake.ObstaclesSystem
{
    [Serializable]
    public class TakingLifeObstacle : Obstacle
    {
        public TakingLifeObstacle()
        {
            Width = 45;
            Height = 48;
        }

        public override int AcquirePoints()
        {
            return 5;
        }

        public override Obstacle Clone()
        {
            return new TakingLifeObstacle();
        }

        public override void Draw(Graphics display)
        {
            Image img = Resources.TakingLife1;
            display.DrawImage(img, Position);
            //display.DrawRectangle(Pens.Red, new Rectangle(Position, new Size(Width, Height)));
            //display.FillRectangle(Brushes.Green, new Rectangle(Position.X, Position.Y, Width, Height));
        }

        public override void Punish(Snake TheSnake)
        {
            TheSnake.TakeLife();
        }
    }
}
