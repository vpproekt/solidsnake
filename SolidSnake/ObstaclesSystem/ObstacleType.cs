﻿using System;

namespace SolidSnake.ObstaclesSystem
{
    [Serializable]
    public enum ObstacleType
    {
        GameOverObstacles,
        TakingLifeObstacle
    }
}
