﻿using SolidSnake.SnakeSystem;
using System;
using System.Drawing;

namespace SolidSnake.ObstaclesSystem
{
    [Serializable]
    public abstract class Obstacle
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public Point Position { get; set; }

        public abstract Obstacle Clone();
        public abstract int AcquirePoints(); 
        public abstract void Punish(Snake TheSnake);
        public abstract void Draw(Graphics display);

        public Rectangle GetRectangle()
        {
            return new Rectangle(Position, new Size(Width, Height));
        }
    }
}
