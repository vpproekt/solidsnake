﻿using SolidSnake.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidSnake
{
    public class GameInfo
    {
        public static void ShowGameResults(int points,int lives, Graphics display,int WindowHeight,int WindowWidth)
        {
            int X = 10;
            int Y = WindowHeight - 1;

            Image imgL = Resources.LifeInfo15x15;
            display.DrawImage(imgL, X,Y + 641);
            string livesStr = ": " + lives.ToString();
            Font font = new Font(FontFamily.GenericSerif, 15, FontStyle.Bold);
            Brush brush = new SolidBrush(Color.White);
            display.DrawString(livesStr, font, brush, X + 15 + 3, Y + 637);

            Image imgP = Resources.PointsInfo20x20;
            display.DrawImage(imgP, X + 57 + 3, Y + 637);
            string pointsStr = ": " + points.ToString();
            display.DrawString(pointsStr, font, brush, X + 75 + 3, Y + 637);
        }
    }
}
