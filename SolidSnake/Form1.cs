﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using SolidSnake.Properties;
using SolidSnake.SnakeSystem;
using SolidSnake.Utils;

namespace SolidSnake
{
    public partial class Form1 : Form
    {
        Game Game;
        int counter;
        private bool IsPaused;

        private bool IsStarted;

        Rectangle currentView;

        Rectangle SnakeStartPosition;

        private bool IsGameOver;
        string FileName;

        public Form1()
        {
            InitializeComponent();
            IsStarted = false;
            IsGameOver = false;
            this.Width = 1200;
            this.Height = 700;
            this.DoubleBuffered = true;
            playButton.Enabled = false;
            playButton.Visible = false;
            counter = 0;
            SnakeStartPosition = new Rectangle(0, 0, 300, 150);
            Game = new Game(Width, Height, SnakeSpeedTimer, SnakeStartPosition);
            CanStartPlay();
            pauseDisplay.Width = Width;
            pauseDisplay.Height = Height;
            currentView = new Rectangle(0, 0, 1183, 660);
            FileName = null;
        }

        public void CanStartPlay()
        {
            if (IsStarted && !IsGameOver)
            {
                //IsStarted = !IsStarted;
                playButton.Enabled = false;
                playButton.Visible = false;
                this.BackgroundImage = Resources.Walpaper3;
                StartNewGame();
            }
            else if (!IsStarted && !IsGameOver)
            {
                playButton.Enabled = true;
                playButton.Visible = true;
                this.BackgroundImage = Resources.StartWallpaper;
            }
            else if (IsGameOver)
            {
                StopTimers();
                this.BackgroundImage = Resources.GameOverWallpaper;
            }
        }

        private void Form1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            if (isPressedStartNewGameButton(e.X, e.Y))
            {
                IsStarted = !IsStarted;
                IsGameOver = !IsGameOver;
                this.BackgroundImage = Resources.Walpaper3;
                StartNewGame();
            }
            else if (isPressedExitButton(e.X, e.Y))
            {
                Application.Exit();
            }
            else if (Game.pressedOpenButtonOnStartWallpaper(e.X, e.Y))
            {
                open();
                if (FileName != null)
                {
                    IsStarted = !IsStarted;
                    CanStartPlay();
                }
            }
            Invalidate(true);
        }

        private void Form1_DoubleClick(object sender, EventArgs e)
        {
            //Game.AddFood();
            Invalidate(true);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            if (IsStarted && !IsGameOver)
            {
                e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                Game.Draw(e.Graphics);
                //e.Graphics.DrawRectangle(Pens.Red, currentView);
            }
            else if (!IsStarted && !IsGameOver)
            {
                Game.FillStartDisplay(e.Graphics, playButton);
            }
            else if (IsGameOver)
            {
                fillGameOverDisplay(e.Graphics);
            }
        }

        public bool isPressedStartNewGameButton(int x, int y)
        {
            int X = currentView.Right / 2 - 150;
            int Y = currentView.Bottom - 100;

            if ((x >= X && x <= X + 164) && (y >= Y && y <= Y + 52)) return true;
            else return false;
        }

        public bool isPressedExitButton(int x, int y)
        {
            int X = currentView.Right / 2 - 150 + 200;
            int Y = currentView.Bottom - 100;

            if ((x >= X && x <= X + 160) && (y >= Y && y <= Y + 52)) return true;
            else return false;
        }

        private void fillGameOverDisplay(Graphics display)
        {
            //display.Clear(Color.White);
            int X = currentView.Right / 2 - 150;
            int Y = currentView.Bottom - 100;

            string PointsStr = "Your total points: " + Game.TotalPoints.ToString();
            Font font = new Font(FontFamily.GenericSansSerif, 20, FontStyle.Bold);
            Brush brush = new SolidBrush(Color.MediumVioletRed);
            display.DrawString(PointsStr, font, brush, X - 70 , Y-170);

            Image start = Resources.ButtonNewGame_new;
            display.DrawImage(start, X, Y);

            Image exit = Resources.ButtonExit_new;
            display.DrawImage(exit, X + 200, Y);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                if (!Game.Snake.checkUpperProximity(currentView)) Game.Snake.changeSnakeDirection(Direction.Up);
            }
            if (e.KeyCode == Keys.Down)
            {
                if (!Game.Snake.checkBottomProximity(currentView)) Game.Snake.changeSnakeDirection(Direction.Down);
            }
            if (e.KeyCode == Keys.Left)
            {
                if (!Game.Snake.checkLeftProximity(currentView)) Game.Snake.changeSnakeDirection(Direction.Left);
            }
            if (e.KeyCode == Keys.Right)
            {
                if (!Game.Snake.checkRightProximity(currentView)) Game.Snake.changeSnakeDirection(Direction.Right);
            }
            if (e.KeyCode == Keys.P)
            {
                if (IsStarted) Paused();
            }
            if (e.KeyCode == Keys.O)
            {
                if (!IsGameOver)
                {
                    if (IsStarted && !IsPaused) Paused();
                    open();
                    if (IsPaused) Paused();
                }
            }
            if (e.KeyCode == Keys.S)
            {
                if (IsStarted && !IsGameOver)
                {
                    if (IsStarted && !IsPaused) Paused();
                    save();
                    if (IsPaused) Paused();
                }
            }
            //Invalidate(true);
        }

        public void save()
        {
            if (FileName == null)
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = " Solid Snake docs|*.snake";
                dialog.Title = "Saving . . . ";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    FileName = dialog.FileName;
                }
            }
            if (FileName != null)
            {
                try
                {
                    using (FileStream stream = new FileStream(FileName, FileMode.Create))
                    {
                        var formatter = new BinaryFormatter();
                        formatter.Serialize(stream, (Game)Game);
                        FileName = null;
                    }
                }
                catch (Exception ex)
                {
                    string st = ex.ToString();
                    MessageBox.Show(st);
                }
            }
        }

        public void open()
        {
            if (FileName == null)
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = " Solid Snake docs|*.snake";
                dialog.Title = "Opening . . . ";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    FileName = dialog.FileName;
                }
            }
            if (FileName != null)
            {
                try
                {
                    using (FileStream stream = new FileStream(FileName, FileMode.Open))
                    {
                        var formatter = new BinaryFormatter();
                        Game = (Game)formatter.Deserialize(stream);
                        RocketGenerator.SetArea(Width, Height);
                        RocketGenerator.SubscribeGame(Game);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR");
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            if ((counter++) % 50 == 0)
            {
                Game.AddFood();
            }

            GameOver();
            //Invalidate(true);
        }

        private void SnakeSpeedTimer_Tick(object sender, EventArgs e)
        {
            Game.Move();
            Game.CheckCollisions();
            Invalidate(true);
        }

        public void GameOver()
        {
            if (Game.isGameOver())
            {
                IsGameOver = !IsGameOver;
                IsStarted = !IsStarted;
                CanStartPlay();
            }
        }

        public void Paused()
        {
            IsPaused = !IsPaused;
            TogglePauseDisplay();
            if (IsPaused)
            {
                pauseDisplay.Invalidate();
                StopTimers();
            }
            else
            {
                StartTimers();
            }
        }

        private void TogglePauseDisplay()
        {
            pauseDisplay.Enabled = !pauseDisplay.Enabled;
            pauseDisplay.Visible = !pauseDisplay.Visible;
        }

        public void StopTimers()
        {
            timer1.Stop();
            SnakeSpeedTimer.Stop();
            FoodsTimer.Stop();
        }

        public void StartTimers()
        {
            timer1.Start();
            SnakeSpeedTimer.Start();
            FoodsTimer.Start();
        }
        private void StartNewGame()
        {
            IsPaused = false;
            EnableTimers();
            Game = new Game(Width, Height, SnakeSpeedTimer, SnakeStartPosition);
            timer1.Start();
            SnakeSpeedTimer.Start();
            FoodsTimer.Start();
        }
        public void EnableTimers()
        {
            timer1.Enabled = true;
            SnakeSpeedTimer.Enabled = true;
            FoodsTimer.Enabled = true;
        }

        private void FoodsTimer_Tick(object sender, EventArgs e)
        {
            Game.UpdateFoodTimes();
            // Invalidate(true);
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isPressedStartNewGameButton(e.X, e.Y) || isPressedExitButton(e.X, e.Y) || Game.pressedOpenButtonOnStartWallpaper(e.X,e.Y))
            {
                Cursor = Cursors.Hand;
            }
            else Cursor = Cursors.Default;
        }

        private void pauseDisplay_Paint(object sender, PaintEventArgs e)
        {
            Game.FillPauseDisplay(e.Graphics);
        }

        private void pauseDisplay_Click(object sender, EventArgs e)
        {
            MouseEventArgs clickEvent = (MouseEventArgs)e;
            if (IsPaused)
            {
                if (Game.pressedContinueButton(clickEvent.X, clickEvent.Y))
                {
                    Paused();
                }
                else if (Game.pressedSaveButton(clickEvent.X, clickEvent.Y))
                {
                    save();
                    if (IsPaused) Paused();
                }
                else if (Game.pressedOpenButton(clickEvent.X, clickEvent.Y))
                {
                    open();
                    if (IsPaused) Paused();
                }

                else if (Game.pressedEndButton(clickEvent.X, clickEvent.Y))
                {
                    Application.Exit();
                }
            }
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            MouseEventArgs clickEvent = (MouseEventArgs)e;
            if (!IsPaused)
            {
                IsStarted = !IsStarted;
                CanStartPlay();
            }
        }

        private void playButton_MouseEnter(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }

        private void playButton_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }

        private void pauseDisplay_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsPaused && (Game.pressedEndButton(e.X, e.Y) || Game.pressedContinueButton(e.X, e.Y) || Game.pressedSaveButton(e.X, e.Y) || Game.pressedOpenButton(e.X, e.Y)))
            {
                Cursor = Cursors.Hand;
            }
            else Cursor = Cursors.Default;
        }
    }
}
