﻿using System;
using System.Drawing;
using SolidSnake.Properties;
using SolidSnake.SnakeSystem;

namespace SolidSnake.FoodSystem
{
    [Serializable]
    public class PowerFood : Food
    {
        public PowerFood() : base()
        {
            RemainingTime = 10;
            InfluenceTime = 10;
            NutritiousPower = 3;
        }
        public override void CancelPower(Snake snake)
        {
            snake.CouldDestroyObstacles = false;
        }


        public override Food Clone()
        {
            return new PowerFood();
        }

        public override void Draw(Graphics display)
        {
            if (!Active)
            {
                Image img = Resources.PowerFood25x25;
                display.DrawImage(img, Position);
                //display.FillRectangle(Brushes.Gold, new Rectangle(Position.X, Position.Y, Width, Height));
            }
        }

        public override void FeedSnake(Snake snake)
        {
            Active = true;
            snake.CouldDestroyObstacles = true;
        }
    }
}
