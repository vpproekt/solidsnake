﻿using System;
using System.Drawing;
using SolidSnake.Properties;
using SolidSnake.SnakeSystem;

namespace SolidSnake.FoodSystem
{
    [Serializable]
    public class SpeedyFood : Food
    {
        public SpeedyFood() : base()
        {
            RemainingTime = 20;
            InfluenceTime = 10;
            NutritiousPower = 0;
        }
        public override void CancelPower(Snake snake)
        {
            snake.NormalizeSpeed();
        }

        public override Food Clone()
        {
            return new SpeedyFood();
        }

        public override void Draw(Graphics display)
        {
            if (!Active)
            {
                Image img = Resources.SpeeadyFood25x25_gromce;
                display.DrawImage(img, Position);
                // display.FillRectangle(Brushes.Green, new Rectangle(Position.X, Position.Y, Width, Height));
            }
        }

        public override void FeedSnake(Snake snake)
        {
            Active = true;
            snake.IncreaseSpeed();
        }
    }
}
