﻿using System;
using System.Drawing;
using SolidSnake.Properties;
using SolidSnake.SnakeSystem;

namespace SolidSnake.FoodSystem
{
    [Serializable]
    public class ShrinkageFood : Food
    {
        public ShrinkageFood() : base()
        {
            RemainingTime = 20;
            InfluenceTime = 5;
            NutritiousPower = -1;
        }
        public override void CancelPower(Snake snake)
        {
            return;
        }

        public override Food Clone()
        {
            return new ShrinkageFood();
        }

        public override void Draw(Graphics display)
        {
            if (!Active)
            {
                Image img = Resources.ShrinkageFood25x25;
                display.DrawImage(img, Position);
                //display.FillRectangle(Brushes.Purple, new Rectangle(Position.X, Position.Y, Width, Height));
            }
        }

        public override void FeedSnake(Snake snake)
        {
            Active = true;
            snake.Shrink();
        }
    }
}
