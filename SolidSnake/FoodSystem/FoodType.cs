﻿using System;

namespace SolidSnake.FoodSystem
{
    [Serializable]
    public enum FoodType
    {
        GrowingFood,
        ShrlnkageFood,
        LifeFood,
        PowerFood,
        SpeedyFood,
        SlowlyFood,
        RocketFood
    }
}
