﻿using System;
using System.Drawing;
using SolidSnake.Properties;
using SolidSnake.SnakeSystem;
using SolidSnake.Utils;

namespace SolidSnake.FoodSystem
{
    [Serializable]
    public class RocketFood : Food
    {
        public RocketFood() : base()
        {
            RemainingTime = 15;
            InfluenceTime = 0;
            NutritiousPower = 0;
        }

        public override void CancelPower(Snake snake)
        {
            return;
        }

        public override Food Clone()
        {
            return new RocketFood();
        }

        public override void Draw(Graphics display)
        {
            if (!Active)
            {
                Image img = Resources.RocketFood;
                display.DrawImage(img, Position);
                //display.FillRectangle(Brushes.Lavender, new Rectangle(Position.X, Position.Y, Width, Height));
            }
        }

        public override void FeedSnake(Snake snake)
        {
            RocketGenerator.GenerateNew();
            Active = true;
        }
    }
}
