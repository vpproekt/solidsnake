﻿using SolidSnake.SnakeSystem;
using System;
using System.Drawing;

namespace SolidSnake.FoodSystem
{
    [Serializable]
    public abstract class Food
    {
        public Point Position { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int NutritiousPower { get; set; }
        public bool Active { get; set; }
        public int RemainingTime { get; set; }
        public int InfluenceTime { get; set; }

        public Food()
        {
            Active = false;
        }


        public abstract void FeedSnake(Snake snake);
        public abstract void Draw(Graphics display);
        public abstract Food Clone();
        public abstract void CancelPower(Snake snake);


        public int AcquirePoints() { return NutritiousPower; }

        public Rectangle GetRectangle()
        {
            return new Rectangle(Position, new Size(Width, Height));
        }
    }
}
