﻿using System;
using System.Drawing;
using SolidSnake.Properties;
using SolidSnake.SnakeSystem;

namespace SolidSnake.FoodSystem
{
    [Serializable]
    public class GrowingFood : Food
    {
        public GrowingFood() : base()
        {
            RemainingTime = 50;
            InfluenceTime = 20;
            NutritiousPower = 1;
        }

        public override void CancelPower(Snake snake)
        {
            return;
        }

        public override Food Clone()
        {
            return new GrowingFood();
        }

        public override void Draw(Graphics display)
        {
            if (!Active)
            {
                Image img = Resources._25x25;
                display.DrawImage(img, Position);
                //display.FillRectangle(Brushes.Blue, new Rectangle(Position.X, Position.Y, Width, Height));
            }
        }

        public override void FeedSnake(Snake snake)
        {
            Active = true;
            snake.Increase();
        }
    }
}
