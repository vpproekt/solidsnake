﻿using System;
using System.Drawing;
using SolidSnake.Properties;
using SolidSnake.SnakeSystem;

namespace SolidSnake.FoodSystem
{
    [Serializable]
    public class LifeFood : Food
    {
        public LifeFood() : base()
        {
            RemainingTime = 15;
            InfluenceTime = 5;
            NutritiousPower = 3;
        }

        public override void CancelPower(Snake snake)
        {
            return;
        }

        public override Food Clone()
        {
            return new LifeFood();
        }

        public override void Draw(Graphics display)
        {
            if (!Active)
            {
                Image img = Resources.LifeFood25x25;
                display.DrawImage(img, Position);
                //display.FillRectangle(Brushes.Lavender, new Rectangle(Position.X, Position.Y, Width, Height));
            }
        }

        public override void FeedSnake(Snake snake)
        {
            Active = true;
            snake.SnakeLifes++;
        }
    }
}
