﻿using System;
using System.Drawing;
using SolidSnake.Properties;
using SolidSnake.SnakeSystem;

namespace SolidSnake.FoodSystem
{
    [Serializable]
    public class SlowlyFood : Food
    {
        public SlowlyFood() : base()
        {
            RemainingTime = 25;
            InfluenceTime = 10;
            NutritiousPower = 0;
        }
        public override void CancelPower(Snake snake)
        {
            snake.NormalizeSpeed();
        }

        public override Food Clone()
        {
            return new SlowlyFood();
        }

        public override void Draw(Graphics display)
        {
            if (!Active)
            {
                Image img = Resources.SlowlyFood25x25;
                display.DrawImage(img, Position);
                //display.FillRectangle(Brushes.DarkRed, new Rectangle(Position.X, Position.Y, Width, Height));
            }
        }

        public override void FeedSnake(Snake snake)
        {
            Active = true;
            snake.DecreaseSpeed();
        }
    }
}
