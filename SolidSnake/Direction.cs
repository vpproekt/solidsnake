﻿using System;

namespace SolidSnake
{
    [Serializable]
    public enum Direction
    {
        Down,
        Up,
        Left,
        Right
    }
}
    