﻿using SolidSnake.SnakeSystem;
using System;
using System.Collections.Generic;
using SolidSnake.FoodSystem;
using SolidSnake.Utils;
using System.Drawing;
using System.Windows.Forms;
using SolidSnake.ObstaclesSystem;
using SolidSnake.Properties;

namespace SolidSnake
{
    [Serializable]
    public class Game
    {
        int Width;
        int Height;
        public int TotalPoints { get; set; }
        public Snake Snake { get; set; }
        public List<Food> Food { get; set; }
        public List<Obstacle> Obstacle { get; set; }
        public List<Rocket> Rockets { get; set; }
        public Rectangle SnakeStartPosition { get; set; }

        public Game(int width, int height,Timer SpeedTimer, Rectangle SnakeStartPosition)
        {
            this.Width = width;
            this.Height = height;
            TotalPoints = 0;
            Snake = new Snake(SpeedTimer);
            this.SnakeStartPosition = SnakeStartPosition;
            Food = new List<Food>();
            Rockets = new List<Rocket>();
            GenerateObstacle(); // Generate 10 obstacle on the form
            RocketGenerator.SetArea(Width, Height);
            RocketGenerator.SubscribeGame(this);
        }

        internal void AddNewRocket(Rocket rocket)
        {
            Rockets.Add(rocket);
        }

        public void GenerateObstacle()
        {
            
            Obstacle = new List<Obstacle>();
            for (int i = 0; i < 10; i++)
            {
                Obstacle obstacle = RandomObstaclesGenerator.GenerateObstacle(Width, Height);
                while (isColidedObstacle(obstacle) || CollisionDetector.CheckCollision(obstacle, SnakeStartPosition))
                {
                    obstacle = RandomObstaclesGenerator.GenerateObstacle(Width, Height);
                }
                Obstacle.Add(obstacle);
            }
        }

        public void GenerateObstacles(int numberOfObstacles)
        {
            if(numberOfObstacles > 0)
            {
                Obstacle obstacle = RandomObstaclesGenerator.GenerateObstacle(Width, Height);
            }
        }

        public bool isColidedObstacle(Obstacle o)
        {
            foreach (Obstacle obstacle in Obstacle)
            {
                if (CollisionDetector.CheckCollision(obstacle, o)) return true;
            }
            return false;
        }

        public void AddFood()
        {
            Food food = RandomFoodGenerator.GenerateFood(Width, Height);
            while (isColidedFood(food))
            {
                food = RandomFoodGenerator.GenerateFood(Width, Height);
            }
            Food.Add(food);
        }

        public bool isColidedFood(Food food)
        {
            foreach (Food f in Food)
            {
                if (CollisionDetector.CheckCollision(f, food)) return true;
            }
            foreach (Obstacle o in Obstacle)
            {
                if (CollisionDetector.CheckCollision(o, food)) return true;
            }
            return false;
        }

        public void UpdateFoodTimes()
        {
            for (int i = 0; i < Food.Count; ++i)
            {
                if (Food[i].Active)
                {
                    Food[i].InfluenceTime--;
                    if (Food[i].InfluenceTime <= 0)
                    {
                        Food[i].CancelPower(Snake);
                        Food.RemoveAt(i);
                    }
                }
                else
                {
                    Food[i].RemainingTime--;
                    if (Food[i].RemainingTime <= 0)
                    {
                        Food.RemoveAt(i);
                    }
                }
            }
        }
        public void FillPauseDisplay(Graphics g)
        {
            int X = Width/2-165;
            int Y = Height/2-70;
            string str = "Paused";
            Font fontStr = new Font(FontFamily.GenericMonospace, 50, FontStyle.Underline);
            Brush brushStr = new SolidBrush(Color.White);
            g.DrawString(str, fontStr, brushStr, X , Y );

            Font font = new Font(FontFamily.GenericSerif, 15, FontStyle.Bold);
            Brush brush = new SolidBrush(Color.White);

            Image imgL = Resources.LifeInfo20x20;
            g.DrawImage(imgL, X + 70 , Y + 84);
            string livesStr = ": " + Snake.SnakeLifes.ToString();
            g.DrawString(livesStr, font, brush, X + 94, Y + 80);

            Image imgP = Resources.PointsInfo20x20;
            g.DrawImage(imgP, X + 134, Y + 82);
            string pointsStr = ": " + TotalPoints.ToString();
            g.DrawString(pointsStr, font, brush, X + 149 + 2, Y + 80);

            Image imgContinue = Resources.ContinueButton;
            Pen p = new Pen(Color.Blue);
            g.DrawImage(imgContinue, X + 59, Y + 119);

            Image imgSave = Resources.button_save;
            g.DrawImage(imgSave, X - 10, Y + 170);

            Image imgOpen = Resources.button_open;
            g.DrawImage(imgOpen, X + 132 + 5, Y + 170);

            Image imgEnd = Resources.EndButton;
             g.DrawImage(imgEnd, X + 90, Y + 220);
        }

        public static void FillStartDisplay(Graphics display, PictureBox playButton)
        {
            int X = 1200 / 2 - 210;
            int Y = 700 / 2 - 150;
            string str = "Solid Snake";
            Font fontStr = new Font(FontFamily.GenericMonospace, 50, FontStyle.Underline, GraphicsUnit.World);
            Brush brushStr = new SolidBrush(Color.White);
            display.DrawString(str, fontStr, brushStr, X, Y);

            playButton.Location = new Point(X + 120, Y + 130);
            playButton.Width = 122;
            playButton.Height = 42;

            Image snake = Resources.Image1;
            display.DrawImage(snake, X - 130, Y - 37);
            display.FillEllipse(Brushes.White, X - 77, Y - 22, 5, 5);
            display.FillEllipse(Brushes.White, X - 96, Y - 20, 4, 4);

            Image snake2 = Resources.ImageMiror;
            display.DrawImage(snake2, X + 353, Y - 37);
            display.FillEllipse(Brushes.White, X + 427, Y - 22, 5, 5);
            display.FillEllipse(Brushes.White, X + 448, Y - 20, 4, 4);

            Image openBut = Resources.OPENbutton;
            display.DrawImage(openBut, X + 670, Y + 408);
        }

        public bool pressedOpenButtonOnStartWallpaper(int x, int y)
        {
            int X = (1200 / 2 - 210) + 670;
            int Y = (700 / 2 - 150) + 408;
            if ((x >= X && x <= X + 116) && (y >= Y && y <= Y + 44)) return true;
            else return false;
        }

        public bool pressedContinueButton(int x,int y)
        {
            int X = (Width / 2 - 165) + 59;
            int Y = (Height / 2 - 70) + 119;
            if ((x >= X && x <= X + 132) && (y >= Y && y <= Y + 45)) return true;
            else return false;
        }
        public bool pressedSaveButton(int x, int y)
        {
            int X = (Width / 2 - 165) - 10;
            int Y = (Height / 2 - 70) + 170;
            if ((x >= X && x <= X + 132) && (y >= Y && y <= Y + 45)) return true;
            else return false;
        }
        public bool pressedOpenButton(int x, int y)
        {
            int X = (Width / 2 - 165) + 132 + 5;
            int Y = (Height / 2 - 70) + 170;
            if ((x >= X && x <= X + 132) && (y >= Y && y <= Y + 45)) return true;
            else return false;
        }
        public bool pressedEndButton(int x, int y)
        {
            int X = (Width / 2 - 165) + 90;
            int Y = (Height / 2 - 70) + 220;
            if ((x >= X && x <= X + 74) && (y >= Y && y <= Y + 45)) return true;
            else return false;
        }
        public void Move()
        {
            Snake.Move(Width, Height);

            // Move all rockets
            for(int i = 0; i < Rockets.Count; ++i)
            {
                Rockets[i].Move();
                // check if the current rocket need to be destroyed
                if(Rockets[i].ShouldBeDestroyed)
                {
                    Rockets.RemoveAt(i);
                }
            }
        }

        public void CheckCollisions()
        {
            for(int i = 0; i < Food.Count; ++i)
            {
                if (!Food[i].Active)
                {
                    if (CollisionDetector.CheckCollision(Snake, Food[i]))
                    {
                        Food[i].FeedSnake(Snake);
                        TotalPoints += Food[i].AcquirePoints();
                    }
                }
            }
            //Add implementation for Checking colision between snake and obstacle
            for (int i = 0; i < Obstacle.Count; ++i)
            {
                if (CollisionDetector.CheckCollision(Snake, Obstacle[i]))
                {
                    if (!Snake.CouldDestroyObstacles)
                    {
                        Obstacle[i].Punish(Snake);
                        if (Snake.SnakeLifes > 0) Obstacle.RemoveAt(i);
                    } else
                    {
                        TotalPoints += Obstacle[i].AcquirePoints();
                        Obstacle.RemoveAt(i);
                    }
                }
            }
        }

        public bool isGameOver()
        {
            return ((Snake.SnakeLifes == 0) || (Snake.IsEatingItself()));
        }

        public void Draw(Graphics display)
        {
            Snake.Draw(display);
            GameInfo.ShowGameResults(TotalPoints, Snake.SnakeLifes, display, 0, 100);
            foreach (var f in Food)
            {
                f.Draw(display);
            }
            //Draw Obstacle on the form
            foreach (var o in Obstacle)
            {
                o.Draw(display);
            }
            // Draw all rockets on screen
            foreach (var r in Rockets)
            {
                r.Draw(display);
            }
        }
    }
}
