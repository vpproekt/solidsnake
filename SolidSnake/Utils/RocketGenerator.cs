﻿using SolidSnake.ObstaclesSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidSnake.Utils
{
    public class RocketGenerator
    {
        private static int Width, Height;
        static Game Game;
        static Random Generator = new Random();

        public static void SetArea(int width, int height)
        {
            Width = width;
            Height = height;
        }

        public static void SubscribeGame(Game game)
        {
            Game = game;
        }

        public static void GenerateNew()
        {
            Rocket rocket = null;

            Direction dir = (Direction)Generator.Next(4);

            int x = 0, y = 0;

            if (dir == Direction.Down)
            {
                x = Generator.Next(0, Width-60);
                y = 0;
            }
            if (dir == Direction.Up)
            {
                x = Generator.Next(0, Width-60);
                y = Height - 20;
            }
            if (dir == Direction.Left)
            {
                x = Width - 20;
                y = Generator.Next(0, Height-60);
            }
            if (dir == Direction.Right)
            {
                x = 0;
                y = Generator.Next(0, Height-60);
            }

            rocket = new Rocket(x, y, Width, Height, dir);
            rocket.IdentifySnake(Game.Snake);

            Game.AddNewRocket(rocket);
        }
    }
}
