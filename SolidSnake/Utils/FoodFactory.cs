﻿using SolidSnake.FoodSystem;
using System.Collections.Generic;

namespace SolidSnake.Utils
{
    public class FoodFactory
    {
        private static readonly List<Food> Foods = new List<Food> {
            new GrowingFood(),
            new ShrinkageFood(),
            new LifeFood(),
            new PowerFood(),
            new SpeedyFood(),
            new SlowlyFood(),
            new RocketFood()
        };
        public static Food CreateFood(FoodType type)
        {
            return Foods[(int)type].Clone();   
        }

    }
}
