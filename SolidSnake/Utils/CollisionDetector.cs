﻿using SolidSnake.SnakeSystem;
using SolidSnake.FoodSystem;
using SolidSnake.ObstaclesSystem;
using System.Drawing;

namespace SolidSnake.Utils
{
    public class CollisionDetector
    {
 
        public static bool CheckCollision(Snake snake, Food food)
        {
            return snake.GetRectangle().IntersectsWith(food.GetRectangle());
        }

        public static bool CheckCollision(Snake snake, Obstacle obstacle)
        {
            return snake.GetRectangle().IntersectsWith(obstacle.GetRectangle());
        }

        public static bool CheckCollision(Obstacle obstacleOne, Obstacle obstacleTwo)
        {
            return obstacleOne.GetRectangle().IntersectsWith(obstacleTwo.GetRectangle());
        }

        public static bool CheckCollision(Obstacle obstacle, Food food)
        {
            return obstacle.GetRectangle().IntersectsWith(food.GetRectangle());
        }

        public static bool CheckCollision(Food foodOne, Food foodTwo)
        {
            return foodOne.GetRectangle().IntersectsWith(foodTwo.GetRectangle());
        }

        public static bool CheckCollision(SnakePart PartOne, SnakePart PartTwo)
        {
            return PartOne.GetRectangle().IntersectsWith(PartTwo.GetRectangle());
        }

        public static bool CheckCollision(Obstacle o, Rectangle r)
        {
            return o.GetRectangle().IntersectsWith(r);
        }
    }
}
