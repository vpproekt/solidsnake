﻿using SolidSnake.FoodSystem;
using System;
using System.Drawing;

namespace SolidSnake.Utils
{
    public class RandomFoodGenerator
    {
        public static Random r = new Random();

        public static Food GenerateFood(int width, int height)
        {
            FoodType type = (FoodType)r.Next(0, 7);
            Food food = FoodFactory.CreateFood(type);

            int x = r.Next(0, width - 65);
            int y = r.Next(0, height - 65);
            food.Position = new Point(x, y);
            food.Height = 25;
            food.Width = 25;

            return food;
        }

    }
}
