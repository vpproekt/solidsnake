﻿using SolidSnake.ObstaclesSystem;
using System.Collections.Generic;

namespace SolidSnake.Utils
{
    public class ObstaclesFactory
    {
        private static readonly List<Obstacle> obstacles = new List<Obstacle> {
            new GameOverObstacles(),
            new TakingLifeObstacle()
        };
        public static Obstacle CreateObstacle(ObstacleType type)
        {
            return obstacles[(int)type].Clone();
        }
    }
}
