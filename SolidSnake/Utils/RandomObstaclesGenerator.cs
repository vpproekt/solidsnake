﻿using SolidSnake.ObstaclesSystem;
using System;
using System.Drawing;

namespace SolidSnake.Utils
{
    public class RandomObstaclesGenerator
    {
        public static Random r = new Random();

        public static Obstacle GenerateObstacle(int width, int height)
        {
            ObstacleType type = (ObstacleType)r.Next(0, 2);
            Obstacle obstacle = ObstaclesFactory.CreateObstacle(type);
            int x = r.Next(0, width - 100);
            int y = r.Next(0, height - 100);
            obstacle.Position = new Point(x, y);
            return obstacle;
        }
    }
}
